<?php 

//This will format the time to be for NZ Auckland time
date_default_timezone_set("Pacific/Auckland");

//Will report all errors
error_reporting(E_ALL);


//the if staement runs a function if it is true
// the stristr will search for the Case-insensitive http_host and yoobee.net.nz
//the ini_set will give the values for the text
if (stristr($_SERVER['HTTP_HOST'], ".yoobee.net.nz")) {
    define("DEV_ENVIRONMENT", false);
    // production environment
    ini_set('display_errors', true);
    ini_set("log_errors", 1);
    ini_set("error_log", getcwd() . "/php-error.log");

    define("MAILGUN_KEY", 'key-520cb269a1776de1998faeaeedbbe71a');
    define("MAILGUN_DOMAIN", 'sandboxc0da4026b6a84a4aa935fed1e9fbc270.mailgun.org');

    define("DB_HOST", 'localhost');
    define("DB_NAME", 'schlocktoberfest');
    define("DB_USER", 'root');
    define("DB_PASS", '');


} else {
    define("DEV_ENVIRONMENT", true);
    // local developer environment
    ini_set('display_errors', true);
    ini_set("log_errors", 1);
    ini_set("error_log", getcwd() . "/php-error.log");

    define("MAILGUN_KEY", 'key-520cb269a1776de1998faeaeedbbe71a');
    define("MAILGUN_DOMAIN", 'sandboxc0da4026b6a84a4aa935fed1e9fbc270.mailgun.org');

    define("DB_HOST", 'localhost');
    define("DB_NAME", 'schlocktoberfest');
    define("DB_USER", 'root');
    define("DB_PASS", '');


}