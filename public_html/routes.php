<?php

//namespace brings in the Controllers folder to the routes.php file
use App\Controllers\HomeController;
use App\Controllers\AuthenticationController;
use App\Controllers\AboutController;
use App\Controllers\ContactController;
use App\Controllers\MoviesController;
use App\Controllers\MovieSuggestController;
use App\Controllers\MerchandiseController;
use App\Models\Exceptions\ModelNotFoundException;
use App\Controllers\ErrorController;
use App\Controllers\CommentsController;
use App\Services\Exceptions\InsufficientPrivilegesException;

//Use will allow us access to using the MovieSuggestSuccessView.php file.
use \App\Views\MovieSuggestSuccessView;

//isset will determine if the $page does exist.
//If it does exist default the page to equal home
$page = isset($_GET['page']) ? $_GET['page'] : 'home';


//Switch will alternate between the cases where $page is something different.

//The controller property will alternate between each controller file to
// show different pages

//Break will end the case otherwise it will keep running and break the web page
try {

    switch ($page) {
    case "home":

        $controller = new HomeController();
        $controller->show();
        
        break;


    case "login":

        $controller = new AuthenticationController();
        $controller->login();

        break;

    case "auth.attempt":

        $controller = new AuthenticationController();
        $controller->attempt();

    break;

    case "register":

        $controller = new AuthenticationController();
        $controller->register();

        break;

    case "auth.create":

        $controller = new AuthenticationController();
        $controller->create();

        break;

    case "auth.store":

        $controller = new AuthenticationController();
        $controller->store();

        break;


    case "logout":

        $controller = new AuthenticationController();
        $controller->logout();

    break;


    case "about":

        $controller = new AboutController();
        $controller->show();

        break;

    case "contact":

        $controller = new ContactController();
        $controller->show();

        break;

    case "movies":

        $controller = new MoviesController();
        $controller->index();

        break;

    case "movie":

        $controller = new MoviesController();
        $controller->show();

        break;

    case "movie.create":

        $controller = new MoviesController();
        $controller->create();

        break;

    case "comment.create":

        $controller = new CommentsController();
        $controller->create();

        break;

    case "moviesuggest":

        $controller = new MovieSuggestController();
        $controller->show();

        break;

		// capture suggestion data

		$expectedVariables = ['title', 'email', 'newsletter'];

		foreach ($expectedVariables as $variable) {
			
			// assume no errors
			$moviesuggest['errors'][$variable] = "";

			if (isset($_POST[$variable])) {
				$moviesuggest[$variable] = $_POST[$variable];
			} else {
				$moviesuggest[$variable] = "";
			}
		}

		// validate suggestion data

		$error = false;

		if (strlen($moviesuggest['title']) == 0) {
			$moviesuggest['errors']['title'] = "A movie title is required.";
			$error = true;
		}
		if (! filter_var($moviesuggest['email'], FILTER_VALIDATE_EMAIL)) {
			$moviesuggest['errors']['email'] = "A valid email address required.";
			$error = true;
		}

		if ($error === true) {
			$_SESSION['suggestmovieerror'] = true;
			$_SESSION['moviesuggest'] = $moviesuggest;
			header("Location: ./#moviesuggest");
			exit();	
		}

		// form is valid

		// redirect user to success page
		header("Location: ./?page=moviesuggestsuccess");

		// send email to suggester

		$suggesterEmail = new SuggesterEmailView($moviesuggest);
		$suggesterEmail->render();

		// send email to event host

		$eventHostEmail = new SuggestionForEventHostEmailView($moviesuggest);
		$eventHostEmail->render();

		exit();

		break;

    case "movie.store":

        $controller = new MoviesController();
        $controller->store();

        break;

    case "movie.edit":

        $controller = new MoviesController();
        $controller->edit();

        break;

    case "movie.update":

            $controller = new MoviesController();
            $controller->update();

        break;

    case "movie.destroy":

        $controller = new MoviesController();
        $controller->destroy();

        break;


	case "moviesuggestsuccess":

		
		$view = new MovieSuggestSuccessView();
		$view->render();

		break;

		case "merchandise";

		$controller= new MerchandiseController();
		$controller->show();

		break;

	default: 
		throw new ModelNotFoundException();

		break;
	}
} catch (ModelNotFoundException $e) {

    $controller = new ErrorController();
    $controller->error404();

} catch (InsufficientPrivilegesException $e) {

    $controller = new ErrorController();
    $controller->error401();

} catch (Exception $e) {

    $controller = new ErrorController();
    $controller->error500($e);

}
