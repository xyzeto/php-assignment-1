<?php

require "../schlocktoberfest-config.inc.php";

require "vendor/autoload.php";

//start or resume an exisiting session
session_start();

//this will be used to ensure the session is running
session_regenerate_id(true);

$auth = new App\Services\AuthenticationService();

App\Views\View::registerAuthenticationService($auth);
App\Controllers\Controller::registerAuthenticationService($auth);


try {
    require "routes.php";
} catch (\App\Models\Exceptions\ModelNotFoundException $e) {
    echo "404 for real!";
}
