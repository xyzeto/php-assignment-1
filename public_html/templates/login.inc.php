<?php
    $errors = $user->errors;
?>
      <div class="row pad-top pad-bottom pad-left">
        <div class="col-xs-12">
          <form method="POST" action="./?page=auth.attempt" class="form-horizontal">
            <h1 class="pad-top">Log In</h1>
            
            <?php if ($error): ?>
              <div class="alert alert-danger col-sm-8" role="alert"><strong>Sorry</strong>, but those details are not registed to WebStop.</div>
            <?php endif; ?>

            <div class="form-group form-group-lg<?php if ($errors['email']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="email" class="control-label">Email Address</label>
                <input id="email" class="form-control input-lg" name="email"
                  placeholder="Email Address"
                  value="<?= $user->email; ?>">
                <div class="help-block"><?= $errors['email']; ?></div>
              </div>
            </div>

            <div class="form-group form-group-lg<?php if ($errors['password']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="password" class="control-label">Password</label>
                <input id="password" class="form-control input-lg" name="password" type="password">
                <div class="help-block"><?= $errors['password']; ?></div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-10">
                <button class="btn btn-lg btn-success">
                  <span class="glyphicon glyphicon-ok"></span> Register
                </button>
              </div>
            </div>
          </form>

        </div>
      </div>