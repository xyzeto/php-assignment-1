      <div class="row pad-top pad-bottom">
        <div class="col-xs-12 pad-top">

        <ol class="breadcrumb">
          <li><a href="./">Home</a></li>
            <li class="active">Reviews</li>
        </ol>

          <h1>Reviews:</h1>

          <?php if (static::$auth->isAdmin()): ?>
            <p>
              <a href="./?page=movie.create" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span> Add a review
              </a>
            </p>
          <?php endif; ?>

          <?php if (count($movies) > 0): ?>

            <ul>
              <?php foreach($movies as $movie): ?>
                <li><a href="./?page=movie&amp;id=<?= $movie->id ?>">
                <?= $movie->title; ?> (<?= $movie->year; ?>)
                </a></li>
              <?php endforeach; ?>
            </ul>

          <?php else: ?>

            <p>Weirdly enough, there are no movies to show you. Spooky.</p>

          <?php endif; ?>

          <?php $this->paginate("./?page=movies", $p, $recordCount, $pageSize, 5); ?>

        </div>
      </div>