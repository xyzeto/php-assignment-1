</div> <!-- This div is stopping the container in the master template -->


 <!--   <div class=" banner-img ">
        
        <h1>About Us:</h1>
        
        <p class="tex-center">Find out about who we are and what we do.
        </p>


    </div>
-->
    
  <div class="container">      


      <div class="row pad-top">
        <div class="col-sm-offset-1 col-sm-10 col-sm-offset-1">
            <h2 class="text-center pad-top pad-bottom">About WebStop:</h2>
            <p class="pad-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam. In porttitor, augue non porttitor viverra, 
            diam nibh tincidunt odio, eget fringilla sapien neque sit amet libero. 
            Fusce ullamcorper sagittis ante, ac posuere orci placerat eu. Lorem ipsum dolor 
            sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam. In porttitor, augue non porttitor viverra, 
            diam nibh tincidunt odio, eget fringilla sapien neque sit amet libero. 
            Fusce ullamcorper sagittis ante, ac posuere orci placerat eu. </p>
        </div>
        
        <div class="col-sm-6 pad-bottom">
            <img class="img-responsive content-center" src="../images/man.jpg">
        </div>

        <div class="col-sm-6 pad-bottom">
            <img class="img-responsive content-center" src="../images/woman.jpg">
        </div>

      </div>
    </div> <!-- This div is stopping the container in the master template -->


  <div class="grey-areas pad-bottom">
    <div class="container">
      <div class="row">
         
        <div class="col-sm-12 pad-top pad-bottom">
        <h2 class="text-center">What we discuss:</h2>
        </div>

        <div class="col-sm-3">
            <img class="img-responsive content-center" src="../images/web1.png">              
            <h4 class="text-center">Web Design:</h4>       
        </div>

        <div class="col-sm-3">
            <img class="img-responsive content-center" src="../images/dev1.png">              
            <h4 class="text-center">Web Development:</h4>                
        </div>

        <div class="col-sm-3">
            <img class="img-responsive content-center" src="../images/cms1.png">              
            <h4 class="text-center">Content Management Systems:</h4>                
        </div>

        <div class="col-sm-3">
            <img class="img-responsive content-center" src="../images/UiUx1.png">              
            <h4 class="text-center">UI/UX Design:</h4>                        
        </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="row pad-top pad-bottom">

            <h2 class="text-center pad-bottom">What people have said:</h2>

        <div class="col-sm-4 content-center">
        <hr>
            <p><i>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam."</i>
            </p>

            <p>Ethan Marcotte, Independent Web Designer.</p>
        <hr>
        </div>

        <div class="col-sm-4 content-center">
        <hr>
            <p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam.</i>
            </p>

            <p>Chris Coyier, Designer at Code Pen.</p>
        <hr>
        </div>

        <div class="col-sm-4 content-center">
        <hr>
            <p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam.</i>
            </p>

            <p>Dan Cederholm, Co-Founder of Dribbble.</p>
        <hr>
        </div>

    </div>
  </div>

  <div class="grey-areas pad-bottom">
    <div class="container">
      <div class="row">
         
        <div class="col-sm-offset-1 col-sm-10 col-sm-offset-1 pad-top pad-bottom">
        <h2 class="text-center pad-bottom">Want to get in touch:</h2>
        
        <p class="pad-bottom"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam. Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam.
        </p>

        <h1 id="email" class="text-center pad-bottom"><a href="mailto:Admin@webstop.com"><u>Admin@webstop.com</u></a></h1>

        </div>


        </div>
      </div>
    </div>
