</div> <!-- This div is stopping the container in the master template -->


    <div class=" banner-img ">
        
        <h1>WebStop</h1>
        
        <p class="text-center">The number one pit stop for all your reviews on open source web technology.
        </p>

        <p>
          <a href="#reviews">
            <button class="btn btn-default btn-lg"> View Reviews </button>
          </a>
        </p>
    </div>


  <div class="container">
      <div class="row pad-top pad-bottom">
        <div class="col-xs-offset-1 col-xs-10 col-xs-offset-1">
            <h2 class="text-center">Welcome to WebStop:</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam. In porttitor, augue non porttitor viverra, 
            diam nibh tincidunt odio, eget fringilla sapien neque sit amet libero. 
            Fusce ullamcorper sagittis ante, ac posuere orci placerat eu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Donec aliquam, nibh eget porta rhoncus, lorem ante tincidunt orci, vitae 
            imperdiet eros neque at diam. In porttitor, augue non porttitor viverra, 
            diam nibh tincidunt odio, eget fringilla sapien neque sit amet libero. 
            Fusce ullamcorper sagittis ante, ac posuere orci placerat eu. </p>
          <a name="reviews"></a>
         
          </div>
      </div>
    </div> <!-- This div is stopping the container in the master template -->


    <div class="grey-areas pad-top pad-bottom">   
  <div class="container">

      <div class="row pad-bottom">
          <div class="col-sm-offset-1 col-sm-10 col-sm-offset-1">
          <h2 class="text-center pad-bottom review-thumbnail">Latest Reviews:</h2>
          </div>
          
          <div class="col-sm-3 pad-bottom">
            <a href=""><img class="img-responsive content-center" src="../images/tnw.jpg"></a>
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/bootstrap-vs-foundation.jpg">
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/jquery.jpg">
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/html-css.jpg">
          </div>


          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/creativeconnect.jpg">
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/materialdesign.jpg">
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/mobile.jpg">
          </div>

          <div class="col-sm-3 pad-bottom">
            <img class="img-responsive content-center" src="../images/monograph.jpg">
          </div>

      </div>
    </div>
  </div>

