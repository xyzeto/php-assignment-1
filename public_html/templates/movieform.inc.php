<?php
    $errors = $movie->errors;
    $verb = ( $movie->id ? "Edit" : "Add" );
    if ($movie->id) {
      $submitAction = "./?page=movie.update";
    } else {
      $submitAction = "./?page=movie.store";
    }
?>

<div class="row pad-top pad-bottom">
<div class="col-xs-12 pad-top pad-bottom">
<form method="POST" action="<?= $submitAction ?>" class="form-horizontal pad-left">          
    
    <?php if($movie->id): ?>
            <input type="hidden" name="id" value="<?= $movie->id ?>">
    <?php endif; ?>

    <h1><?= $verb; ?> Movie</h1>      
            <div class="form-group<?php if ($errors['title']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="title" class="control-label">Movie Title</label>
                <input id="title" class="form-control input-lg" name="title" 
                  placeholder="Troll 2"
                  value="<?= $movie->title; ?>">
                <div class="help-block"><?= $errors['title']; ?></div>
              </div>
            </div>

            <div class="form-group <?php if ($errors['year']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="year" class="control-label">Release Year</label>
                <input id="year" class="form-control input-lg" name="year" size="5"
                  placeholder="1990"
                  value="<?= $movie->year; ?>">
                <div class="help-block"><?= $errors['year']; ?></div>
              </div>
            </div>

            <div class="form-group <?php if ($errors['description']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="description" class="control-label">Description</label>
                <textarea id="description" class="form-control" name="description" rows="5"
                  placeholder="A paragraph about the movie."><?= $movie->description; ?></textarea>
                <div class="help-block"><?= $errors['description']; ?></div>
              </div>
            </div>

            <div class="form-group form-group-lg<?php if ($errors['tags']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="tags" class="control-label">Tags</label>
                <div id="tags" class="form-control"></div>
                <script>
                  var inputTags = "<?= $movie->tags ?>";
                </script>
                <div class="help-block"><?= $errors['tags']; ?></div>
              </div>
            </div>
        
              <button class="btn btn-success btn-pad-bottom">
                <span class="glyphicon glyphicon-ok"></span> <?= $verb; ?> Movie
              </button>
        </form>

          <?php if ($movie->id): ?>
            <form method="POST" action="./?page=movie.destroy" class="form-horizontal pad-left">
              <div class="form-group">
                  <input type="hidden" name="id" value="<?= $movie->id ?>">
                  <button class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span> Delete Movie
                  </button>
              </div>
            </form>
          <?php endif; ?>

        </div>
      </div>