      <div class="row">
        <div class="col-xs-12">
          <h1><u>Merchandise</u></h1>

          <?php if (count($merchandise) > 0): ?>

            <?php foreach($merchandise as $item): ?>
              <h3><?= $item->name; ?> – $<?= $item->price; ?></h3>
              <p><?= $item->description; ?></p>
            <?php endforeach; ?>

          <?php else: ?>

            <p>No merchandise found. Sorry.</p>

          <?php endif; ?>

        </div>
      </div>