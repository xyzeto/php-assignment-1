<?php
    $errors = $user->errors;
?>
      <div class="row pad-top pad-bottom pad-left">
        <div class="col-sm-10 col-xs-12">
          <form method="POST" action="./?page=auth.store" class="form-horizontal">
            <h1 class="pad-top">Register New User</h1>

            <div class="form-group form-group-lg<?php if ($errors['email']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="email" class="control-label">Email Address</label>
                <input id="email" class="form-control input-lg" name="email"
                  placeholder="Enter an email address"
                  value="<?= $user->email; ?>">
                <div class="help-block"><?= $errors['email']; ?></div>
              </div>
            </div>


            <div class="form-group form-group-lg<?php if ($errors['password']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="password" class="control-label">Password</label>
                <input id="password" class="form-control input-lg" name="password" type="password" placeholder="Enter a password of your choice">
                <div class="help-block"><?= $errors['password']; ?></div>
              </div>
            </div>

            <div class="form-group form-group-lg<?php if ($errors['password2']): ?> has-error <?php endif; ?>">
              <div class="col-sm-8">
              <label for="password2" class="control-label">Confirm Password</label>
                <input id="password2" class="form-control input-lg" name="password2" type="password" placeholder="Re-enter your password">
                <div class="help-block"><?= $errors['password2']; ?></div>
              </div>
            </div>


            <div class="form-group">
              <div class="col-sm-8 pad-bottom">
                <button class="btn btn-lg btn-success">
                  <span class="glyphicon glyphicon-ok"></span> Register
                </button>
              </div>
            </div>
          </form>

        </div>
      </div>


