<?php
$errors = $newcomment->errors;
?>

      <div class="row pad-top pad-bottom">
        <div class="col-xs-12 pad-top">

        <ol class="breadcrumb">
          <li><a href="./">Home</a></li>
          <li><a href="./?page=movies">Reviews</a></li>
          <li class="active"><?= $movie->title; ?></li>
        </ol>

          <h1><?= $movie->title; ?></h1>
          <p>Released in <?= $movie->year; ?></p>
          <p><?= $movie->description; ?></p>
          
          <ul class="list-inline">
          <?php foreach($tags as $tag): ?><!--
            --><li><span class="label label-default"><?= $tag->tag ?></span></li><!--
          --><?php endforeach; ?>
          </ul>

          <?php if (static::$auth->isAdmin()): ?>
          <p>
            <a href="./?page=movie.edit&id=<?= $movie->id ?>" class="btn btn-primary">
              <span class="glyphicon glyphicon-pencil"></span> Edit Movie
            </a>
          </p>
          <?php endif; ?>


          <h2>Comments</h2>
          <?php if (count($comments) > 0): ?>
            <?php $count = 0; ?>
            <?php foreach($comments as $comment): ?>
              <?php $count += 1; ?>
              <article id="comment-<?= $comment->id ?>" class="media">
                <div class="media-left">
                  <img src="<?= $comment->user()->gravatar(48) ?>" alt="">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">#<?= $count ?> <?= $comment->user()->username ?></h4>
                  <p><?= $comment->comment ?></p>
                </div>
              </article>
            <?php endforeach; ?>
          <?php else: ?>
            <p>No comments. Yet…</p>
          <?php endif; ?>


        <h3>Add Comment to '<?= $movie->title ?>'</h3>
          <?php if (static::$auth->check()): ?>
            <form method="POST" action="./?page=comment.create" class="form-horizontal">
              <input type="hidden" name="movie_id" value="<?= $movie->id ?>">

              <div class="form-group <?php if ($errors['comment']): ?> has-error <?php endif; ?>">
                <div class="col-sm-8">

                  <textarea id="comment" class="form-control" name="comment" rows="5"><?= $newcomment->comment ?></textarea>
                  <div class="help-block"><?= $errors['comment']; ?></div>
                </div>
              </div>

              <div class="form-group pad-bottom">
                <div class="col-sm-8">
                  <button class="btn btn-success">
                    <span class="glyphicon glyphicon-ok"></span> Add Comment
                  </button>
                </div>
              </div>
            </form>

          <?php else: ?>
            <p>You need to be <a href="./?page=login">logged in</a> to add a comment.</p>
          <?php endif; ?>          

        </div>
      </div>