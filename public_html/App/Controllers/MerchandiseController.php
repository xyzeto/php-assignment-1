<?php

namespace App\Controllers;

//The use method will force the page to only run if it can access the 
//Movie.php and MoviesView.php files
use App\Models\Merchandise;
use App\Views\MerchandiseView;

class MerchandiseController extends Controller
{
    public function show()
    {
    	//The ::all(); will show all the data
        $merchandise = Merchandise::all();

        $view = new MerchandiseView(['merchandise' => $merchandise]);
        $view->render();
    }
}