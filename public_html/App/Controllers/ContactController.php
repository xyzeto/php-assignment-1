<?php

namespace App\Controllers;

use \App\Views\ContactView;


class ContactController extends Controller
{
    public function show() {
        $view = new ContactView();
        $view->render();
    }
}