<?php

namespace App\Models;

class Merchandise extends DatabaseModel
{
    
    protected static $columns = ['id', 'name', 'description', 'price'];

    protected static $tableName = "merchandise";

}