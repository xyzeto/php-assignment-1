<?php

namespace App\Views;

class Error401View extends TemplateView
{
    public function render()
    {
        $page = "error401";
        $page_title = "401 Internal Server Error";
        include "templates/master.inc.php";
    }

    protected function content()
    {
        extract($this->data);
        include "templates/error401.inc.php";
    }
}