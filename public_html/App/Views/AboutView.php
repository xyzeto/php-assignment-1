<?php

namespace App\Views;


//this class will take whatever is in the TemplateView class in 
//the TemplateView.php file
class AboutView extends TemplateView
{

    //The pubic function render() will 
    //spit out the master content that is 
    //to appear on each page (header and footer)
    //It will also run the $page,$page_title and include master.inc.php
    public function render()
    {
        $page = "about";
        $page_title = "About The Festival";
        include "templates/master.inc.php";
    }

    //The protected function content will
    //spit out the about.inc.php file. Here is what we will
    // have place the content on the page
    protected function content()
    {
        include "templates/about.inc.php";
    }
}
