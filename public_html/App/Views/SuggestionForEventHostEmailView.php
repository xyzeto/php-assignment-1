<?php

namespace App\Views;


class SuggestionForEventHostEmailView extends EmailView
{

    public function render()
    {
        $this->sendEmail("templates/suggestionforeventhostemail.inc.php");
    }
}
