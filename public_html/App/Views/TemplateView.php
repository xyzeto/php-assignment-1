<?php

//This page will use the class templateView and
//carries the properties over from the view class

namespace App\Views;


//The class TemplateView will extend the view class in the View.php file
abstract class TemplateView extends View
{
	//In the TemplateView class, the content function will run
    abstract protected function content();
}
